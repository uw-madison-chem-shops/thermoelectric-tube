# Thermoelectric Tube

A small project for students to put together as a learning exercise.
Involves machining, glass blowing, and electronics work.

![photo](./coverart.jpg)

## Repository

This is an open source hardware project licensed under the CERN Open Hardware License Version 2 - Permissive.
Please see the LICENSE file for the complete license.

